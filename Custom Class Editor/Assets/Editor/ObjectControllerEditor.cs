﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor(typeof(ObjectController))]
public class ObjectControllerEditor : Editor
{
    ObjectController controller;
    
    void Awake()
    {
        controller = (ObjectController)target;
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();
        
        // Pull the array off of ObjectController and store here for use
        SerializedProperty controllerArray = serializedObject.FindProperty("controllerObjects");
        EditorGUILayout.PropertyField(controllerArray);
        if (controllerArray.isExpanded)
        {
            EditorGUI.indentLevel++;
            // Show a property field for the array size
            EditorGUILayout.PropertyField(controllerArray.FindPropertyRelative("Array.size"));
            EditorGUI.indentLevel++;
            for (int i = 0; i < controllerArray.arraySize; i++)
            {
                EditorGUILayout.PropertyField(controllerArray.GetArrayElementAtIndex(i));
            }
            EditorGUI.indentLevel -= 2;
        }
        serializedObject.ApplyModifiedProperties();
    }
}