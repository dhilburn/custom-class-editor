﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomPropertyDrawer(typeof(ObjectTypes))]
public class ObjectTypesDrawer : PropertyDrawer
{
    const float Y_OFFSET = 17f;
    const float LABEL_OFFSET = 35f;
    const float HEIGHT = 17f;
    ObjectTypes thisObject;
    float extraHeight = 55f;
    int shouldSolidMove = 0;

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        EditorGUI.BeginProperty(position, label, property);
        // Display custom items
            SerializedProperty objectType = property.FindPropertyRelative("type");
            SerializedProperty breakPoints = property.FindPropertyRelative("breakablePoints");
            SerializedProperty isSolidMoving = property.FindPropertyRelative("solidMoving");
            SerializedProperty solidStartPos = property.FindPropertyRelative("solidStart");
            SerializedProperty solidEndPos = property.FindPropertyRelative("solidEnd");
            SerializedProperty damageType = property.FindPropertyRelative("damageType");
            SerializedProperty damageAmount = property.FindPropertyRelative("damageAmount");
            SerializedProperty healType = property.FindPropertyRelative("healingType");
            SerializedProperty healPickup = property.FindPropertyRelative("healingPickupType");
            SerializedProperty healAmt = property.FindPropertyRelative("healingAmount");

            Rect labelRect = new Rect(position.x, position.y, position.width * 0.33f, HEIGHT);
            EditorGUI.LabelField(labelRect, "Object type");
            Rect objectTypeDisplay = new Rect(position.x + position.width * 0.33f, position.y, position.width * 0.67f, HEIGHT);           
            EditorGUI.PropertyField(objectTypeDisplay, objectType, GUIContent.none);

            //Debug.Log(objectType.enumValueIndex);

            switch((ObjectType)objectType.enumValueIndex)
            {
                case(ObjectType.BREAKABLE):
                    Rect breakableRect = new Rect(position.x, position.y + Y_OFFSET, position.width, HEIGHT);
                    EditorGUI.PropertyField(breakableRect, breakPoints);
                    break;
                case(ObjectType.DAMAGING):
                    float xOffset = position.x;
                    Rect damageLabelRect = new Rect(xOffset, position.y + Y_OFFSET, 72f, HEIGHT);
                    EditorGUI.LabelField(damageLabelRect, "Type");
                    xOffset += 72f;
                    Rect damageTypeRect = new Rect(xOffset, position.y + Y_OFFSET, position.width / 3, HEIGHT);
                    EditorGUI.PropertyField(damageTypeRect, damageType, GUIContent.none);
                    xOffset += position.width / 3;
                    Rect damageAmountLabel = new Rect(xOffset, position.y + Y_OFFSET, 84f, HEIGHT);
                    xOffset += 84f;
                    EditorGUI.LabelField(damageAmountLabel, "Damage amount");
                    Rect damageAmountRect = new Rect(xOffset, position.y + Y_OFFSET, position.width / 3, HEIGHT);
                    EditorGUI.PropertyField(damageAmountRect, damageAmount, GUIContent.none);
                break;
                case(ObjectType.HEALING):

                    break;
                case(ObjectType.PASSABLE):

                    break;
                case(ObjectType.SOLID):

                    break;
            }
        EditorGUI.EndProperty();
    }

    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        return base.GetPropertyHeight(property, label) + extraHeight;
    }
}